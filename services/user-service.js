var  User = require('../models/user').User;

exports.addUser = function(user, next){
  
  var newUser = User({
      
     firstName: user.firstName,
     email: user.email,
     password: user.password
  });  
  
  newUser.save(function(err){
     if(err){
         return next(err);
     } 
      next(null);   
  });
    
};